************************************************************
** Stata margins in a Nutshell
** Examples
** Arne Bethmann - 18.4.2016
************************************************************
set more off

** Preparation:
** 1. Create a working directory
** 2. Create a "do" folder in your working directory
** 3. Save this do file in the "do" folder
** 4. Create a "dta" folder in your working directory
** 5. Copy the ALLBUS data to the "dta" folder
** 6. Create a "log" folder in your working directory
** 7. Edit the next line as indicated
*global path "<fill in your working directory here>"

log using "$path\log\a_prep.log", replace text
********************
** A Prepare data
********************
** The data used here are from ALLBUS 2012:
** http://www.gesis.org/allbus

** Load raw data (ALLBUS 2012)
use "$path\dta\ZA4614_v1-1-1.dta", clear

** ID variable
gen id=v2
label variable id "Identifikationsnummer des Befragten"
isid id

** Gender role attitudes
clonevar item1=v102
recode item1 0 8 9 =.
tab v102 item1, m

clonevar item2=v103
recode item2 0 8 9=.		
tab v103 item2, m

clonevar item3=v104
recode item3 0 8 9=.		
tab v104 item3, m

clonevar item4=v105
recode item4 0 8 9=.		
tab v105 item4, m

clonevar item5=v106
recode item5 0 8 9=.		
tab v106 item5, m

clonevar item6=v107
recode item6 0 8 9=.		
tab v107 item6, m

** Dummy east/west Germany
rename v8 ostwest
tab ostwest, m

** Gender
rename v217 geschl
tab geschl, m

** Age
clonevar alter=v220
recode alter 999=.
tab v220 alter, m

** Marital status
clonevar familie=v274
recode familie 99 =.
tab v274 familie, m

** School education
clonevar schul=v230
recode schul 7 99 =.
tab v230 schul, m

** Vocational training
gen beruf = .
label variable beruf "Berufsbildungsabschluss"
replace beruf = 0 if inrange(v232,0,1) | inrange(v233,0,1) | inrange(v234,0,1) ///
  | inrange(v235,0,1) | inrange(v236,0,1) | inrange(v237,0,1) | inrange(v238,0,1) ///
  | inrange(v239,0,1) | inrange(v240,0,1) | inrange(v241,0,1) | inrange(v242,0,1)
replace beruf = 1 if v233 == 1 | v234 == 1 | v236 == 1 | v237 == 1 | v238 == 1
replace beruf = 2 if v239 == 1 | v240 == 1
label define beruf 0 "kein Abschluss" 1 "Ausbildung o. �." 2 "Hochschulabschluss"
label value beruf beruf
tab beruf, m

** Number of minors in HH (w/o respondent)
egen anz_mindj = anycount(v357 v367 v377 v387 v397 v407 v417), values(0/17)
label variable anz_mindj "Anzahl minderj�hriger Personen im HH(ohne befragte Person)"
tab anz_mindj, m

** Number of children under 3 in HH
egen anz_u3j = anycount(v357 v367 v377 v387 v397 v407 v417), values(0/2)
label variable anz_u3j "Anzahl Kinder unter 3 im HH"
tab anz_u3j, m

** Net income
gen income = v346 if v346 != 99997 & v346 != 99999
sum income, d
replace income = . if income > 3500 | income == 0
hist income

** Arbeitslosigkeit
gen alo = v259 == 3 if v259 != 9
tab alo

** "Armut"
gen arm = income < 720 
tab arm

keep id item* geschl alter schul beruf familie anz_mindj anz_u3j ostwest income arm
order id item* geschl alter schul beruf familie anz_mindj anz_u3j ostwest income arm

save "$path\dta\work.dta", replace
********************
log close


log using "$path\log\b_ex.log", replace text
********************
** B Examples
********************

** Load dataset
use "$path\dta\work.dta", clear

** Income by gender
graph bar income, over(geschl)
ttest income, by(geschl)
regress income i.geschl

** Income by age
twoway  (scatter income alter, jitter(5))(lfit income alter, lwidth(1))
regress income c.alter

** Income by age and gender
twoway (lfit income alter if geschl == 1, lwidth(1)) ///
  (lfit income alter if geschl == 2, lwidth(1))
  
** Simple Regression
regress income i.geschl c.alter
regress income i.geschl##c.alter

** Margins
margins geschl
margins geschl, dydx(alter)
margins, dydx(alter)

** Marginsplot
margins geschl, at(alter=(20(5)65))
marginsplot

margins, dydx(geschl) at(alter=(20(5)65))
marginsplot

** Add complexity
set more off

regress income i.geschl##c.alter##c.alter##c.alter ///
  i.geschl##i.schul i.geschl##i.beruf

margins geschl, at(alter=(20(2)65))
marginsplot
margins, dydx(beruf)
margins geschl, dydx(beruf)

margins, dydx(geschl) at(alter=(20(2)65))
marginsplot


** Nonlinear models
logit arm i.schul i.beruf i.geschl c.alter##c.alter 
logit, or
margins, dydx(schul beruf geschl alter)

** Complex case
logit arm i.schul##c.alter##c.alter i.beruf##c.alter##c.alter ///
  i.geschl##c.alter##c.alter i.geschl##i.schul i.geschl##i.beruf 
margins, dydx(schul beruf geschl alter)

margins beruf, at(alter=(20(2)65))
marginsplot

margins, dydx(beruf) at(alter=(20(2)65))
marginsplot

** Different modelling strategies
quietly: logit arm i.schul##c.alter##c.alter i.beruf##c.alter##c.alter ///
  i.geschl##c.alter##c.alter i.geschl##i.schul i.geschl##i.beruf 
margins, dydx(schul beruf geschl alter) cformat(%8.4f)

quietly: probit arm i.schul##c.alter##c.alter i.beruf##c.alter##c.alter ///
  i.geschl##c.alter##c.alter i.geschl##i.schul i.geschl##i.beruf 
margins, dydx(schul beruf geschl alter) cformat(%8.4f)

quietly: regress arm i.schul##c.alter##c.alter i.beruf##c.alter##c.alter ///
  i.geschl##c.alter##c.alter i.geschl##i.schul i.geschl##i.beruf 
margins, dydx(schul beruf geschl alter) cformat(%8.4f)


** Other nonlinear models
tab item4
mean item4, over(geschl)
ologit item4 i.geschl
ologit item4 i.geschl##c.alter##c.alter##c.alter i.schul i.beruf c.income##c.income
margins, dydx(geschl alter schul beruf)

margins geschl, at(alter=(20(2)65))
marginsplot

margins, dydx(geschl) at(alter=(20(2)65))
marginsplot


********************
log close
