# Stata's `margins` in a Nutshell
### Oder: Average Marginal Effects in 90 Minuten

[Stata do-File](do/margins90_examples.do)

Die Beispiele in diesem Stata do-File sollen eine 90-Minuten-Einführung in die Nutzung des `margins`-Befehls geben. Ziel ist es zu zeigen wie vorhergesagte Werte und Average Marginal Effects bei der Interpretation von nicht-linearen Regressionsmodellen helfen können. Dazu wird sowohl auf Interaktionseffekte als auch verallgemeinerte lineare Modelle (logistische Regression etc.) eingegangen.

Verbesserungsvorschläge sind natürlich immer willkommen!

## Vorbereitung
Die Kursteilnehmer sollten über eine Stata-Installation (> 12.0) verfügen und sich
die Daten des ALLBUS 2012 im Stata-Format bei der [Gesis](http://www.gesis.org/allbus) 
heruntergeladen haben. Zu Beginn müssen die Schritte unter *Preparation* im [do-File](do/margins90_examples.do) durchgeführt werden. Der erste Teil des do-Files bereitet dann einen kleinen 
Arbeitsdatensatz auf. Der zweite Teil beinhaltet die Beispiele zum Durchklicken. Im Anschluss können die Beispiele dann gut als Grundlage zum eigenen 
Ausprobieren genutzt werden.

## Literatur
Die einschlägigen Stata-Manuals findet man hier:
* [margins](http://www.stata.com/manuals14/rmargins.pdf)
* [marginsplot](http://www.stata.com/manuals14/rmarginsplot.pdf)

Etwas lesbarere Einführungstexte:
* [Williams, R. (2012): Using the margins command to estimate and interpret adjusted predictions and marginal effects. *The Stata Journal*, 12(2), 308-331](http://www.stata-journal.com/sjpdf.html?articlenum=st0260)
* [Best, H. und Wolf, C. (2012): Modellvergleich und Ergebnisinterpretation in Logit- und Probit-Regressionen. *Kölner Zeitschrift für Soziologie und Sozialpsychologie*, 64(2), 377-395](http://link.springer.com/article/10.1007/s11577-012-0167-4)

Eine Schritt für Schritt Erklärung mit vielen Beispielen und Bildern:
* [Mitchell, M. N. (2012): Interpreting and Visualizing Regression Models Using Stata. College Station, TX: Stata Press](http://www.stata.com/bookstore/interpreting-visualizing-regression-models/)

In diesem Zusammenhang auch interessant:
* [Ai, C. und Norton, E. C. (2003): Interaction terms in logit and probit models. *Economics Letters*, 80, 123-129](https://pdfs.semanticscholar.org/6285/8e64d9a337504d72cb862c4cc1e7fd27a7a0.pdf)
* [Mood, C. (2010): Logistic Regression: Why We Cannot Do What We Think We Can Do, and What We Can Do About It. *European Sociological Review*, 26(1), 67-82](https://academic.oup.com/esr/article-pdf/26/1/67/1439459/jcp006.pdf)

## Lizenz
![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)

"Stata's "margins" in a Nutshell - Oder: Average Marginal Effects in 90 Minuten" von Arne Bethmann ist lizenziert unter einer [Creative Commons Namensnennung 4.0 International Lizenz](http://creativecommons.org/licenses/by/4.0/).